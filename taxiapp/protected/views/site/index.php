<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>

<h1>Hello <i><?= (Yii::app()->user->isGuest ? 'Guest' : Yii::app()->user->getName()) ?></i></h1>
<p>Congratulations! You have successfully created your Yii application.</p>
<div id="app">
    <form>
        <div class="form-group">
            <label>Date</label>
            <input type="text" name="dateRange" v-model="dateRange" class="form-control date-widget">
        </div>
        <div class="form-group">
            <label>Group By</label>
            <select name="groupBy" v-model="groupBy" class="form-control">
                <option value="request_ip">BY IP</option>
            </select>
        </div>
        <input type="button" @click="getLogs()" value="Search" class="btn btn-success">
    </form>

    <table class="table table-hover">
        <tr>
            <th>ID</th>
            <th>Date</th>
            <th>IP</th>
        </tr>
        <tr v-for='log in logs'>
            <td>{{ log.id }}</td>
            <td>{{ log.request_date }}</td>
            <td>{{ log.request_ip }}</td>
        </tr>
    </table>
</div>
