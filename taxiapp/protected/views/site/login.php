<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm */

$this->pageTitle = 'Login Page';
$this->breadcrumbs = array(
    'Login',
);
?>

<h1>Login</h1>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="row">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'username'); ?>
            <?php echo $form->textField($model, 'username', ['class' => 'form-control']); ?>
            <?php echo $form->error($model, 'username'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'password'); ?>
            <?php echo $form->passwordField($model, 'password', ['class' => 'form-control']); ?>
            <?php echo $form->error($model, 'password'); ?>
        </div>
    </div>

    <div class="row rememberMe">
        <div class="form-group">
            <?php echo $form->checkBox($model, 'rememberMe'); ?>
            <?php echo $form->label($model, 'rememberMe'); ?>
            <?php echo $form->error($model, 'rememberMe'); ?>
        </div>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Login', ['class' => 'btn btn-success']); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
