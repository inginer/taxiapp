<?php

class ApiController extends Controller
{
    public function actionLog()
    {
        header('Content-type: application/json');
        $data = [];
        $isSuccess = true;
        $errorMsg = "";

        try {
            $searchModel = new ServerLog();
            $searchModel->setAttributes($_GET);
            $dataProvider = $searchModel->search();
            $data = $dataProvider->getData();

        } catch (Exception $exception) {
            $isSuccess = false;
            $errorMsg = $exception->getMessage();
        }

        echo CJSON::encode([
            'success' => $isSuccess,
            'data' => $data,
            'error' => $errorMsg,
        ]);
        Yii::app()->end();
    }
}
