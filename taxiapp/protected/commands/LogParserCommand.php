<?php

class LogParserCommand extends CConsoleCommand
{
    public function actionParse()
    {
        $lines = file(Yii::app()->params['logFile']);
        $logFormat = Yii::app()->params['logFormat'];
        $parser = new \BenMorel\ApacheLogParser\Parser($logFormat);

        $fields = $parser->getFieldNames();

        foreach ($lines as $line) {
            try {
                $parsedLine = array_combine($fields, $parser->parse($line, true));
                $ip = $parsedLine['remoteHostname'];
                $dateTimeParse = substr(substr(
                    $parsedLine['remoteUser'],
                    strpos($parsedLine['remoteUser'], '['),
                    strrpos($parsedLine['remoteUser'], ']')
                ), 1, -8);

                $date = str_replace('/','-',substr($dateTimeParse, 0, strpos($dateTimeParse, ':')));
                $time = substr($dateTimeParse, strpos($dateTimeParse, ':') + 1);

                $model = new ServerLog();
                $model->request_ip = $ip;
                $model->request_date = date('Y-m-d H:i:s', strtotime($date.' '.$time));
                $model->save();

            } catch (Exception $exception) {

            }
        }
    }
}
