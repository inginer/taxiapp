<?php

/**
 * This is the model class for table "server_log".
 *
 * The followings are the available columns in table 'server_log':
 * @property string $id
 * @property string $request_ip
 * @property string $request_date
 */
class ServerLog extends CActiveRecord
{
    public $groupBy;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'server_log';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('request_ip, request_date', 'required'),
            array('request_ip', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, request_ip, request_date,groupBy', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'request_ip' => 'Request Ip',
            'request_date' => 'Request Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('request_ip', $this->request_ip, true);

        if (!empty($this->request_date)) {
            $expDate = array_map('trim', explode('-', $this->request_date));
            $dateStart = date('Y-m-d', strtotime($expDate[0]));
            $dateTo = date('Y-m-d', strtotime($expDate[1]));

            $criteria->addBetweenCondition('request_date', $dateStart, $dateTo);
        }

        if (!empty($this->groupBy)) {
            $criteria->group = $this->groupBy;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 100,
            ],
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ServerLog the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
