Yii Web Programming Framework
=============================

Thank you for choosing Yii - a high-performance component-based PHP framework.


INSTALLATION
------------
```
git clone https://gitlab.com/inginer/taxiapp.git

cd taxiapp && composer install
```

IMPORT MYSQL
------------
Создаем БД импортируем sql 
```
taxiapp/protected/data/*.sql
```

QUICK START
-----------
```
php -S localhost:8080
```
IMPORT DATA
-----------
```
cd taxiapp/protected/

php yiic logparser parse
```